from connector import Connector


def get_status_widget_data():
    newsConnector = Connector('symphony', 'newsFeeds')
    sourcesConnector = Connector('symphony', 'newsSources')

    document = {
        'newsAmount': newsConnector.get_collection_count(),
        'sourcesAmount': sourcesConnector.get_collection_count()
    }
    return document

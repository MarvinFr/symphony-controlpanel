from database import MongoManager
import datetime



class Connector:

    def __init__(self, moduleDatabase, moduleCollection):
        self.database = moduleDatabase
        self.collection = moduleCollection
        self.mongoManager = MongoManager(self.database, self.collection)

    def get_collection_count(self):
        return self.mongoManager.mongoClient.get_database(self.database).get_collection(self.collection).count()




from database import MongoManager
import array as array


class SymphonySearch:

    def __init__(self, query):
        self.query = query

        self.feedManager = MongoManager("symphony", "newsFeeds")
        self.sourceManager = MongoManager("symphony", "newsSources")
        self.resultsArray = []

    def search(self):
        array_query = self.query.split()
        sorted_query = []
        # sort search request

        # first add nouns
        for item in array_query:
            if str(item[0]).isupper():
                sorted_query.append(item)

        query_results = []
        document_array = self.feedManager.get_sorted_documents()

        #for document in document_array:
        #    for item in sorted_query:
        #        if str(item) in str(document['title']) or str(document['description']):
        #            source_document = self.sourceManager.get_document_by_key("sik", document['sik'])
        #            new_document = self.to_document(document['link'], document['title'], document['description'],
        #                                            document['pubDate'], document['image'], source_document['category'],
        #                                            source_document['language'], source_document['priority'],
        #                                            source_document['source'])



        # add other words
        #for array_query_item in array_query:
        #    if str(array_query_item) not in sorted_query:
        #        if len(str(array_query_item)) >= 3:
        #            sorted_query.append(array_query_item)

        #print("Sorted query: " + str(sorted_query))
        # --> sorted_query finished

        results = []
        # get all documents
        for document in self.feedManager.get_sorted_documents():
            # TODO: algo

            for item in array_query:

                if str(item) in str(document['title']):
                    results.append(document)
                if str(item) in str(document['description']):
                    results.append(document)

            # if array_query[0] in document['title'] or document['description']:
            # print('The document contains the first argument of the query')
            #   self.resultsArray.append(document)
        print('Your array_query: ' + str(array_query[0]))
        return results

    def to_document(self, link, title, description, pubDate, image, category, language, priority, source):
        document = {
            'link': link,
            'title': title,
            'description': description,
            'pubDate': pubDate,
            'image': image,
            'category': category,
            'language': language,
            'priority': priority,
            'source': source,
            'matches': 0
        }
        return document
# print('Document: ' + str(document))

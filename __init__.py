import os
import sys
from flask import Flask, render_template, redirect, url_for, request, session

from connector import data_fetcher
from search import search_manager
from user import user_manager

sys.path.append('/usr/bin/python3.6/site-packages')

app = Flask(__name__)
app.secret_key = '42679559898758813540'
UPLOAD_FOLDER = 'static/assets/image/profile/'
ALLOWED_EXTENSIONS = {'jpg', 'jpeg', 'gif'}
app.secret_key = os.urandom(16)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/')
def base_dir():
    return redirect(url_for('login'))


@app.route('/login/', methods=['GET', 'POST'])
def login():
    error = None
    return render_template('login/index.html', title='Login', error=error)
    if 'username' in session:
        document = user_manager.get_user(session.get('username'))

        # DataFetcher
        widgetData = data_fetcher.get_status_widget_data()
        return render_template('dashboard/index.html', user=document, stats=widgetData)

    elif request.method == 'POST':
        if user_manager.login(request.form['username'], request.form['password']):
            print('User exists! login..')
            session['username'] = request.form['username']
            user_manager.login_history(request.form['username'], request.environ['REMOTE_ADDR'])
            return redirect(url_for('dashboard'))
        else:
            error = 'Invalid password or username!'

    # userController.add_user("Lol", "Marvin")
    return render_template('login/index.html', title='Login', error=error)


@app.route('/addUser/', methods=['GET', 'POST'])
def user():
    success = None
    if request.method == 'POST':
        if user_manager.username_or_email_exists(request.form['username'], request.form['email']):
            print('User already exists')
            return render_template('user/index.html', sucess='user already exists')
        user_manager.add_user(request.form['username'], request.form['name'], request.form['email'],
                              request.form['password'])
        document = user_manager.get_user(request.form['username'])
        file = request.files['profile']
        #image = Image.open(file)
        #image.thumbnail((400, 400))
        #image.save(UPLOAD_FOLDER + str(document['uuid']) + '.jpg')

        # success = 'Added %s' % request.form['username'] % '; E-Mail: %s' % request.form['email'] % '; Password: %s' % request.form['password']
    return render_template('user/index.html', sucess='success')


@app.route('/stats/')
def stats():
    document = data_fetcher.get_status_widget_data()
    return render_template('stats/index.html', status=document)


@app.route('/test/')
def test():
    if 'username' in session:
        document = user_manager.get_user(session.get('username'))
        # DataFetcher
        widgetData = data_fetcher.get_status_widget_data()
        return render_template('dashboard/sources.html', user=document, stats=widgetData)
    return 'Error not logged in'


# Dashboard

@app.route('/dashboard')
def dashboard():
    if 'username' in session:
        document = user_manager.get_user(session.get('username'))

        # DataFetcher
        widgetData = data_fetcher.get_status_widget_data()
        return render_template('dashboard/dashboard.html', user=document, stats=widgetData)
    return render_template('login/index.html', title='Login')


@app.route('/sources')
def sources():
    if 'username' in session:
        document = user_manager.get_user(session.get('username'))

        return render_template('dashboard/sources.html', user=document)
    return 'Error not logged in'


@app.route('/search', methods=['GET', 'POST'])
def search():
    if 'username' not in session:
        print("Der Username ist nicht teil der session!")
        return 'Error not logged in'

    document = user_manager.get_user(session.get('username'))

    if request.method == 'POST':
        print("Die Request method ist POST!")
        query = request.form.get('searchrq')
        results = search_manager.search(query)
        print("Deine Suchanfrage: " + query)
        return render_template('dashboard/search.html', user=document, query=query, results=results)

    print("Die Request method ist nicht POST!")
    return render_template('dashboard/search.html', user=document)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


#if __name__ == '__main__':
    #app.run()
#    app.run(host='45.82.120.134')

from pymongo import MongoClient
import pymongo

class MongoManager:

    def __init__(self, database, collection):
        uri = "mongodb://localhost:27017/?readPreference=primary"
        self.mongoClient = MongoClient(uri)

        self.database = database
        self.collection = collection

    def insert_document_if_not_exists(self, document):
        self.mongoClient.get_database(self.database).get_collection(self.collection).update(document, document,
                                                                                            upsert=True)
        self.mongoClient.close()

    def insert_one_document(self, document):
        self.mongoClient.get_database(self.database).get_collection(self.collection).insert_one(document)
        self.mongoClient.close()

    def delete_document(self, document):
        self.mongoClient.get_database(self.database).get_collection(self.collection).delete_one(document)

    def document_exists(self, document):
        if self.mongoClient.get_database(self.database).get_collection(self.collection).find_one(document):
            self.mongoClient.close()
            return True
        self.mongoClient.close()
        return False

    def get_collection(self):
        return self.mongoClient.get_database(self.database).get_collection(self.collection)

    def get_document(self, document):
        result = self.mongoClient.get_database(self.database).get_collection(self.collection).find_one(document)
        self.mongoClient.close()
        return result

    def get_document_by_key(self, ident, key):
        result = self.mongoClient.get_database(self.database).get_collection(self.collection).find_one({ident: key})
        self.mongoClient.close()
        return result

    def get_sorted_documents(self):
        result = self.mongoClient.get_database(self.database).get_collection(self.collection).find({}).sort('pubDate', pymongo.DESCENDING)
        self.mongoClient.close()
        return result

import hashlib
import os
import uuid
from time import strftime

from database import MongoManager


def create_uuid():
    user_id = str(uuid.uuid4())
    mongoManager = MongoManager('symphonyWebservice', 'users')
    while mongoManager.document_exists({'uuid': user_id}) is True:
        user_id = str(uuid.uuid4())
    return user_id


class User:

    def __init__(self, username, name, e_mail, password):
        self.uuid = create_uuid()
        self.username = username
        self.name = name
        self.e_mail = e_mail

        # test = datetime.datetime.now()

        self.created_at = strftime('%Y-%m-%d')
        self.salt = os.urandom(32)
        self.password = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), self.salt, 100000)

    def add_user(self):
        mongoManager = MongoManager('symphonyWebservice', 'users')
        mongoManager.insert_document_if_not_exists(self.to_document())

    def delete_user(self):
        mongoManager = MongoManager('symphonyWebservice', 'users')
        mongoManager.delete_document(self.to_document())

    def get_user_by_document(self, document):
        mongoManager = MongoManager('symphonyWebservice', 'users')
        return mongoManager.get_document(document)

    @staticmethod
    def get_user_by_username(username):
        mongoManager = MongoManager('symphonyWebservice', 'users')
        return mongoManager.get_document_by_key('username', username)

    def to_document(self):
        document = {
            'uuid': self.uuid,
            'username': self.username,
            'name': self.name,
            'email': self.e_mail,
            'createdAt': self.created_at,
            'password': self.password,
            'salt': self.salt
        }
        return document

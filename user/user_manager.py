import hashlib

from connector import MongoManager
from user import User
import time


def add_user(username, name, e_mail, password):
    user = User(username, name, e_mail, password)
    user.add_user()


def username_or_email_exists(username, e_mail):
    mongoManager = MongoManager('symphonyWebservice', 'users')
    if mongoManager.document_exists({'username': username}) or mongoManager.document_exists({'email': e_mail}):
        return True
    return False


def login(username, input_password):
    document = User.get_user_by_username(username)
    if document is None:
        return False
    key = document['password']
    salt = document['salt']

    new_key = hashlib.pbkdf2_hmac('sha256', input_password.encode('utf-8'), salt, 100000)
    if key != new_key:
        return False
    return True


def get_user(username):
    return User.get_user_by_username(username)


def login_history(username, ip):
    userDocument = User.get_user_by_username(username)

    login_history_document = {
        'uuid': userDocument['uuid'],
        'ip': ip,
        'timestamp': int(round(time.time() * 1000))
    }
    mongoManager = MongoManager('symphonyWebservice', 'login_history')
    mongoManager.insert_one_document(login_history_document)
